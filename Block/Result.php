<?php

namespace Hunters\Search\Block;

use \Magento\Framework\View\Element\Template;

class Result extends \Magento\Framework\View\Element\Template
{
    protected $_request;
    protected $_pdfCollection;

    public function __construct(
        Template\Context $context,
	    \Magento\Framework\App\RequestInterface $request,
        \Hunters\Pdf\Model\ResourceModel\Pdf\CollectionFactory $pdfCollection,
        array $data = [])
    {
        $this->_pdfCollection = $pdfCollection;
    	$this->request = $request;
        parent::__construct($context, $data);
    }

    public function getPdfFile() {
        $searchCode = $this->_getSearchCode();
        $collection = $this->_pdfCollection->create()
            ->addFieldToSelect(array('code', 'filename'))
            ->addFieldToFilter('code', ['like' => $searchCode]);
        return $collection;
    }

    protected  function _getSearchCode() {
        if ($this->request->getPost()) {
            $postData = $this->request->getPost();
            if (!empty($postData['search_file'])) {
                return "%".$postData['search_file']."%";
            }
        }
    }

	public function getDownloadableUrl($fileName) {
		return "<a href='https://cbd.huntersconsult.com/pub/pdf/files/".$fileName."'>".$fileName."</a>";
	}
}
